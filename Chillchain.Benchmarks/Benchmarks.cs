using System.Collections;
using System.Diagnostics;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Order;
using Chillchain.Node.Blocks;

namespace Chillchain.Benchmarks;

[MemoryDiagnoser]
[Orderer(SummaryOrderPolicy.FastestToSlowest)]
[RankColumn]
public class Benchmarks
{
    private static byte[] Combine(byte[][] arrays)
    {
        var bytes = new byte[arrays.Sum(a => a.Length)];
        var offset = 0;

        foreach (var array in arrays)
        {
            Buffer.BlockCopy(array, 0, bytes, offset, array.Length);
            offset += array.Length;
        }

        return bytes;
    }

    public static byte[] Combine(byte[] first, byte[] second)
    {
        var bytes = new byte[first.Length + second.Length];
        Buffer.BlockCopy(first, 0, bytes, 0, first.Length);
        Buffer.BlockCopy(second, 0, bytes, first.Length, second.Length);
        return bytes;
    }

    public static byte[] CombineLinq(byte[] first, byte[] second)
    {
        return first.Concat(second).ToArray();
    }

    public static byte[] CombineConcats(byte[][] arrays)
    {
        var bytes = Array.Empty<byte>();

        foreach (var b in arrays)
        {
            bytes.Concat(b);
        }

        return bytes.ToArray();
    }

    public static byte[] ConcatSelectMany(byte[][] arrays)
    {
        return arrays.SelectMany(x => x).ToArray();
    }


    public static IEnumerable CombineYield(byte[] first, byte[] second)
    {
        foreach (var b in first)
        {
            yield return b;
        }

        foreach (var b in second)
        {
            yield return b;
        }
    }

    private readonly byte[] first = System.Text.Encoding.UTF8.GetBytes("This is the first list.");
    private readonly byte[] second = System.Text.Encoding.UTF8.GetBytes("And we have another list we'll concatenate.");

    private readonly Block block = new Block("0x00", new DateTime(2022, 6, 7), "This is the data from the block.");

    [Benchmark]
    public void HashBlock()
    {
        block.GetHashCode();
    }

    [Benchmark]
    public void PrintBlock()
    {
        block.ToString();
    }

    // [Benchmark]
    // public void TestCombineArrayOfArrays()
    // {
    //     Combine(new byte[][] { first, second });
    // }
    //
    // [Benchmark]
    // public void TestCombine()
    // {
    //     Combine(first, second);
    // }
    //
    // [Benchmark]
    // public void TestCombineConcats()
    // {
    //     CombineConcats(new byte[][] { first, second });
    // }
    //
    // [Benchmark]
    // public void TestCombineYield()
    // {
    //     CombineYield(first, second);
    // }
}