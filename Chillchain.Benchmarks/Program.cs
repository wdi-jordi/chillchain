﻿// See https://aka.ms/new-console-template for more information

using BenchmarkDotNet.Running;
using Chillchain.Node.Blocks;
using Chillchain.Benchmarks;

Block block = new Block("0x00", new DateTime(2022, 6, 7), "This is the data from the block.");
Console.WriteLine("0x" + block.ToString());
// BenchmarkRunner.Run<Benchmarks>();