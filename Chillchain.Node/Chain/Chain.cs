using Chillchain.Node.Blocks;

namespace Chillchain.Node.Chain;

public class Chain
{
    private static readonly Block GenesisBlock = new("0x0", DateTime.MinValue, "Genesis");

    public Dictionary<string, Block> Ledger { get; } = new() { { GenesisBlock.GetAddress(), GenesisBlock } };

    public Block Mine(string data)
    {
        var block = new Block(Ledger.Last().Value.GetAddress(), DateTime.Now, data);
        Ledger.Add(block.GetAddress(), block);
        return block;
    }
}