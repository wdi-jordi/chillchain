using System.Security.Cryptography;
using System.Text;
using Chillchain.Node.Infrastructure.Extensions;

namespace Chillchain.Node.Blocks;

public readonly struct Block
{
    public string PreviousBlock { get; }
    public DateTime Timestamp { get; }
    public string Data { get; }

    public Block(string previousBlock, DateTime timestamp, string data)
    {
        PreviousBlock = previousBlock;
        Timestamp = timestamp;
        Data = data;
    }

    public new readonly byte[] GetHashCode()
    {
        var previousBlockBytes = Encoding.UTF8.GetBytes(PreviousBlock);
        var dataBytes = Encoding.UTF8.GetBytes(Data);

        return SHA256.Create()
            .ComputeHash(new MemoryStream(previousBlockBytes.CombineWith(dataBytes).ToArray()));
    }

    public readonly override string ToString()
    {
        var hash = GetHashCode();
        var sb = new StringBuilder();
        foreach (var b in hash)
        {
            sb.Append(b.ToString("X"));
        }

        return sb.ToString();
    }

    public readonly string GetAddress() => ToString();
}