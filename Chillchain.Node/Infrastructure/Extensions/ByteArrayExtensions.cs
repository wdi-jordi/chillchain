using System.Collections;

namespace Chillchain.Node.Infrastructure.Extensions;

public static class ByteArrayExtensions
{
    public static IEnumerable<byte> CombineWith(this byte[] first, byte[] second)
    {
        foreach (var b in first)
        {
            yield return b;
        }

        foreach (var b in second)
        {
            yield return b;
        }
    }
    
}