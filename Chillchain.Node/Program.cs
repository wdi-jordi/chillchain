using Chillchain.Node.Chain;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddSingleton<Chain>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapGet("/ledger", (Chain chain) => Results.Ok(chain.Ledger));
app.MapPost("/add-block", (Chain chain, string data) =>
{
    var block = chain.Mine(data);
    return Results.Ok(block);
});

app.Run();